#!/bin/bash
#SBATCH --account=mh0066
#SBATCH --partition=compute
#SBATCH --time=04:00:00

set -o errexit -o nounset -o xtrace

weights_file='weights/weights_hpz10_global_1x1.nc'

if [[ ! -f $weights_file ]]; then
  cdo \
    -f nc4 \
    -gennn,global_1x1 \
    -const,1,hpz7 \
    "${weights_file}"
fi

year=$1

cdo \
  -f nc4 \
  -P 8 \
  -remap,global_1x1,"${weights_file}" \
  -hpdegrade,nside=128 \
  -monmean \
  -mergetime \
  data/icon_historical_${year}*.grib2 \
  data/icon_historical_monmean_1x1_${year}.nc
