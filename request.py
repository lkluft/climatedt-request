#!/home/m/m300575/.conda/envs/polytope/bin/python
#SBATCH --account=mh0066
#SBATCH --partition=shared
#SBATCH --time=02:00:00
#SBATCH --array=1991-2009%3
"""Retrieve DestinE data via the polytope client."""

import argparse
import calendar
import os
from pathlib import Path

from polytope.api import Client
from polytope.api.helpers import HTTPResponseError


_params = {
    "surface_temperature": dict(param="130", levtype="sfc"),
    "mean_sea_level_pressure": dict(param="151", levtype="sfc"),
    "2m_temperautre": dict(param="167", levtype="sfc"),
    "precipitation": dict(param="260048", levtype="sfc"),
}


def download(year=1992):
    client = Client(
        address="polytope.lumi.apps.dte.destination-earth.eu",
    )

    for month in range(1, 13):
        _, last_day = calendar.monthrange(year, month)

        request = {
            "activity": "CMIP6",
            "class": "d1",
            "dataset": "climate-dt",
            "date": f"{year}{month:02d}01/to/{year}{month:02d}{last_day:02d}",
            "experiment": "hist",
            "expver": "0001",
            "generation": "1",
            "model": "ICON",
            "realization": "1",
            "resolution": "high",
            "stream": "clte",
            "time": "00/06/12/18",
            "type": "fc",
            "param": "130/151/167/260048",
            "levtype": "sfc",
        }

        try:
            outfile = f"data/icon_historical_{year}{month:02d}.grib2"

            if Path(outfile).is_file():
                print(f"{outfile} already exists.", flush=True)
                continue

            files = client.retrieve(
                "destination-earth",
                request=request,
                output_file=outfile,
                append=False,
            )
        except HTTPResponseError as err:
            print(err, flush=True)


def main():
    # parser = argparse.ArgumentParser()
    # parser.add_argument('--year', '-y', type=int, default=1992)
    # args = parser.parse_args()

    year = int(os.environ.get("SLURM_ARRAY_TASK_ID", "1992"))

    download(year=year)


if __name__ == "__main__":
    main()
