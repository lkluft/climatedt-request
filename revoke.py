#!/home/m/m300575/.conda/envs/polytope/bin/python
"""Revoke all pending requests via the polytope client."""

from polytope.api import Client


def revoke(request_id="all"):
    client = Client(address="polytope.lumi.apps.dte.destination-earth.eu")
    client.revoke(request_id)


if __name__ == "__main__":
    revoke()
